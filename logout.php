<?php
include 'inc/session.php';

unset($_SESSION['user_id']);
unset($_SESSION['user_nombre']);
unset($_SESSION['user_apellido']);
unset($_SESSION['user_usuario']);

session_destroy();
header('Location: index.php');

?>