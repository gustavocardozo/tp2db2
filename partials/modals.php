<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Salir del sistema</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">¿Está seguro que desea salir del sistema?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-primary" href="logout.php">Si, desconectarme</a>
            </div>
        </div>
    </div>
</div>
<div id="alert-template" type="text/template" style="display: none">
    <div class="alert alert-danger" role="alert">
        {0}
    </div>
</div>
<div id="success-template" type="text/template" style="display: none">
    <div class="alert alert-success" role="alert">
        {0}
    </div>
</div>