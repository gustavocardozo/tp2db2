<?php
include 'inc/session.php';
include 'inc/connection.php';

// Realizando una consulta SQL
$query = "SELECT CONCAT(u1.nombre, ' ', u1.apellido) as user1, CONCAT(u2.nombre, ' ', u2.apellido) as user2
FROM Usuario u1, Usuario u2
WHERE NOT EXISTS 
	(SELECT u1.id 
	FROM (SELECT p.id_pelicula FROM pelis_que_vio p	WHERE p.id_usuario = u1.id) p1 
	FULL OUTER JOIN (SELECT p.id_pelicula FROM pelis_que_vio p WHERE p.id_usuario = u2.id) p2
	ON p1.id_pelicula = p2.id_pelicula
	WHERE p1.id_pelicula IS NULL OR p2.id_pelicula IS NULL) 
	AND u1.id < u2.id;";
$result = pg_query($query);
if (!$result) {
    throw new Exception('La consulta fallo: ' . pg_last_error());
}

$pares = pg_fetch_all($result);
$pares = $pares ? $pares : [];
?>

<!DOCTYPE html>
<html lang="en">

<?php include('partials/head.php'); ?>

  <body id="page-top">

  <?php include('partials/header.php'); ?>

    <div id="wrapper">

        <?php include('partials/menu.php'); ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Usuarios que vieron EXACTAMENTE las mismas películas</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Usuario 1</th>
                      <th>Usuario 2</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Usuario 1</th>
                        <th>Usuario 2</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php foreach ($pares as $par) { ?>
                      <tr>
                          <td><?php echo $par['user1']; ?></td>
                          <td><?php echo $par['user2']; ?></td>
                      </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

          <?php include('partials/footer.php'); ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

  <?php include('partials/modals.php'); ?>

  <?php include('partials/scripts.php'); ?>

  </body>

</html>
