<?php
include '../inc/connection.php';

try {
    if (!isset($_REQUEST['nombre']) || !isset($_REQUEST['apellido']) ||!isset($_REQUEST['usuario']) ||!isset($_REQUEST['password'])) {
        throw new Exception('Parámetros incorrectos');
    }
    if (empty($_REQUEST['nombre']) || empty($_REQUEST['apellido']) ||empty($_REQUEST['usuario']) ||empty($_REQUEST['password'])) {
        throw new Exception('Todos los parámetros son requeridos');
    }

    $nombre = $_REQUEST['nombre'];
    $apellido = $_REQUEST['apellido'];
    $usuario = $_REQUEST['usuario'];
    $clave = md5($_REQUEST['password']);

    // Realizando una consulta SQL
    $query = "INSERT INTO usuario(nombre, apellido, usuario, clave) VALUES('". $nombre . "','". $apellido."', '". $usuario."', '". $clave ."')";
    $result = pg_query($query);
    if (!$result) {
        throw new Exception('La consulta fallo: ' . pg_last_error());
    }


    session_start();
    $_SESSION['user_id'] = pg_last_oid($result);
    $_SESSION['user_nombre'] = $nombre;
    $_SESSION['user_apellido'] = $apellido;
    $_SESSION['user_usuario'] = $usuario;
    echo json_encode(array('success' => true, 'redirecTo' => 'index.html'));

}catch (Exception $exception) {
    echo json_encode(array('success' => false, 'message' => $exception->getMessage()));
}


?>