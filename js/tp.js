$(document).ready(function() {
    var danger = '<div class="alert alert-danger" role="alert">\n' +
        '        {0}\n' +
        '    </div>';
    var success = '<div class="alert alert-success" role="alert">\n' +
        '        {0}\n' +
        '    </div>';

    function showError(error) {
        var container = $('#message-container');
        if (container.length) {
            var content = danger.replace('{0}',error);
            container.append(content);
            var clear = setInterval(function () {
                $('#message-container').html('');
            }, 5000);
        }
    }

    function showSuccess(error) {
        var container = $('#message-container');
        if (container.length) {
            var content = success.replace('{0}',error);
            container.append(content);
            var clear = setInterval(function () {
                $('#message-container').html('');
            }, 5000);
        }
    }



    $("#login-form").submit(function( event ) {
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "ajax/login.php",
            dataType: "json",
            data: {
                user: $("#login-form #inputUser").val(),
                pass: $("#login-form #inputPassword").val()
            }
        })
            .done(function( data ) {
                if (data.success) {
                    location.href = 'index.php'
                } else {
                    showError(data.message);
                }
            });
        //alert( "Handler for .submit() called." );

    });

    $("#register-form").submit(function( event ) {
        if ($("#register-form #password").val() != $("#register-form #confirmPassword").val()) {
            alert('Las contraseñas deben coincidir');
            return false;
        }

        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "ajax/register.php",
            dataType: "json",
            data: {
                nombre: $("#register-form #nombre").val(),
                apellido: $("#register-form #apellido").val(),
                usuario: $("#register-form #usuario").val(),
                password: $("#register-form #password").val(),
            }
        })
            .done(function( data ) {
                if (data.success) {
                    location.href = 'index.php'
                } else {
                    showError(data.message);
                }
            });
        //alert( "Handler for .submit() called." );

    });

    $("#configuration-form").submit(function( event ) {
        if ($("#configuration-form #password").val() != $("#configuration-form #confirmPassword").val()) {
            alert('Las contraseñas deben coincidir');
            return false;
        }

        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "ajax/configuration.php",
            dataType: "json",
            data: {
                nombre: $("#configuration-form #nombre").val(),
                apellido: $("#configuration-form #apellido").val(),
                usuario: $("#configuration-form #usuario").val(),
                password: $("#configuration-form #password").val(),
            }
        })
            .done(function( data ) {
                if (data.success) {
                    showSuccess('Datos guardados correctamente');
                } else {
                    showError(data.message);
                }
            });
        //alert( "Handler for .submit() called." );

    });
});
