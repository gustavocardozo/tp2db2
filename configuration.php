<?php
include 'inc/session.php';
?>
<!DOCTYPE html>
<html lang="en">

<?php include('partials/head.php'); ?>

  <body id="page-top">

  <?php include('partials/header.php'); ?>

    <div id="wrapper">

        <?php include('partials/menu.php'); ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Configuración</li>
          </ol>

          <div class="card card-register mx-auto mt-5">
            <div class="card-header">Datos personales</div>
            <div class="card-body">
                <div id="message-container"></div>
              <form id="configuration-form">
                  <div class="form-group">
                  <div class="form-row">
                    <div class="col-md-6">
                      <div class="form-label-group">
                        <input type="text" id="nombre" class="form-control" placeholder="Nombre" required="required" autofocus="autofocus" value="<?php echo $_SESSION['user_nombre'] ?>">
                        <label for="nombre">Nombre</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-label-group">
                        <input type="text" id="apellido" class="form-control" placeholder="Apellido" required="required" value="<?php echo $_SESSION['user_apellido'] ?>">
                        <label for="apellido">Apellido</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-label-group">
                    <input type="usuario" id="usuario" class="form-control" placeholder="Usuario" required="required" value="<?php echo $_SESSION['user_usuario'] ?>">
                    <label for="usuario">Usuario</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                    <div class="col-md-6">
                      <div class="form-label-group">
                        <input type="password" id="password" class="form-control" placeholder="Clave" required="required">
                        <label for="password">Clave</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-label-group">
                        <input type="password" id="confirmPassword" class="form-control" placeholder="Confirmar clave" required="required">
                        <label for="confirmPassword">Confirmar clave</label>
                      </div>
                    </div>
                  </div>
                </div>
                  <button type="submit" class="btn btn-primary btn-block">Actualizar</button>
              </form>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

          <?php include('partials/footer.php'); ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

  <?php include('partials/modals.php'); ?>

    <?php include('partials/scripts.php'); ?>
    <script src="js/tp.js"></script>

  </body>

</html>
